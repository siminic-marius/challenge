-- create user and grant privileges
CREATE USER 'enrollment'@'localhost' IDENTIFIED BY 'enrollment';
GRANT ALL PRIVILEGES ON * . * TO 'enrollment'@'localhost';

-------------------------------------------------------------------------------------------------- FIRST DATABASE
CREATE DATABASE IF NOT EXISTS `db1` DEFAULT CHARACTER SET latin1;

SET FOREIGN_KEY_CHECKS = 0;

-- students table
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  FULLTEXT KEY (first_name, last_name),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO students(first_name, last_name, mobile) values("Justin", "Smith", "0742314423");
INSERT INTO students(first_name, last_name, mobile) values("Electra", "York", "0723232444");
INSERT INTO students(first_name, last_name, mobile) values("Din", "Cimes", "0767233442");

-- courses table
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `course_code` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`course_code`),
  UNIQUE KEY `NAME_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO courses(course_code, name) values("DB101", "Introduction to Databases");
INSERT INTO courses(course_code, name) values("ALGO201", "Algorithms");
INSERT INTO courses(course_code, name) values("MOB301", "Mobile applications");

-- enrollment table
DROP TABLE IF EXISTS `enrollment`;
CREATE TABLE `enrollment` (
  `student_id` BIGINT NOT NULL AUTO_INCREMENT,
  `course_code` varchar(45) NOT NULL,
  `date_enrolled` date NOT NULL,
  `final_grade` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`course_code`),

  CONSTRAINT `FK_COURSE_CODE` FOREIGN KEY (`course_code`)
  REFERENCES `courses` (`course_code`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,

  CONSTRAINT `FK_STUDENT` FOREIGN KEY (`student_id`)
  REFERENCES `students` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (1, "DB101", "2017-06-15", "A");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (1, "ALGO201", "2018-06-11", "A");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (2, "ALGO201", "2021-03-17", "B");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (3, "ALGO201", "2017-06-15", "C");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (1, "MOB301", "2019-03-13", "A-");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (2, "MOB301", "2018-02-10", "D");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (3, "MOB301", "2016-01-11", "F");

SET FOREIGN_KEY_CHECKS = 1;



------------------------------------------------------------------------------------------------ SECOND DATABASE
CREATE DATABASE IF NOT EXISTS `db2` DEFAULT CHARACTER SET latin1;
SET FOREIGN_KEY_CHECKS = 0;

-- students table
DROP TABLE IF EXISTS `students`;
CREATE TABLE `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  FULLTEXT KEY (first_name, last_name),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

INSERT INTO students(first_name, last_name, mobile) values("Sanjit", "Patel", "0741233123");

-- courses table
DROP TABLE IF EXISTS `courses`;
CREATE TABLE `courses` (
  `course_code` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`course_code`),
  UNIQUE KEY `NAME_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO courses(course_code, name) values("DB101", "Introduction to Databases");
INSERT INTO courses(course_code, name) values("ALGO201", "Algorithms");
INSERT INTO courses(course_code, name) values("MOB301", "Mobile applications");

-- enrollment table
DROP TABLE IF EXISTS `enrollment`;
CREATE TABLE `enrollment` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_code` varchar(45) NOT NULL,
  `date_enrolled` date NOT NULL,
  `final_grade` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`student_id`,`course_code`),

  CONSTRAINT `FK_COURSE_CODE` FOREIGN KEY (`course_code`)
  REFERENCES `courses` (`course_code`)
  ON DELETE NO ACTION ON UPDATE NO ACTION,

  CONSTRAINT `FK_STUDENT` FOREIGN KEY (`student_id`)
  REFERENCES `students` (`id`)
  ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (1, "DB101", "2017-06-15", "C");
INSERT INTO enrollment(student_id, course_code, date_enrolled, final_grade) VALUES (1, "ALGO201", "2018-06-11", "A");


SET FOREIGN_KEY_CHECKS = 1;