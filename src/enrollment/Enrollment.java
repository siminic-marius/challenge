package enrollment;

import java.time.LocalDate;

public class Enrollment {
    private long studentId;
    private String courseCode;
    private LocalDate localDate;
    private String finalGrade;

    public Enrollment(long studentId, String courseCode, LocalDate localDate, String finalGrade) {
        this.studentId = studentId;
        this.courseCode = courseCode;
        this.localDate = localDate;
        this.finalGrade = finalGrade;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public String getFinalGrade() {
        return finalGrade;
    }

    public void setFinalGrade(String finalGrade) {
        this.finalGrade = finalGrade;
    }

    @Override
    public String toString() {
        return "Enrollment{" +
                "studentId=" + studentId +
                ", courseCode='" + courseCode + '\'' +
                ", localDate=" + localDate +
                ", finalGrade='" + finalGrade + '\'' +
                '}';
    }
}
