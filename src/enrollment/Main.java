package enrollment;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Main {

    //    Write a code snippet that queries the SQL server to list all the courses that a student is taking. The second function is already implemented for you.
    private Course[] getCourses(String studentName) throws Exception {
        Course[] course = null;
        if (studentName != null && !studentName.isEmpty()) {
            String query = "SELECT courses.course_code, courses.name FROM courses " +
                    "JOIN enrollment on courses.course_code = enrollment.course_code " +
                    "JOIN students on enrollment.student_id = students.id where MATCH(first_name, last_name) AGAINST(" + "\"" + studentName + "\"" + ")";
            if ('A' <= studentName.charAt(0) && studentName.charAt(0) <= 'M') {
                course = executeGetCoursesSql(database1, query);
            } else {
                course = executeGetCoursesSql(database2, query);
            }
        } else {
            throw new Exception("Argument studentName is null or empty.");
        }
        return course;
    }

    //provided helper function
    private Course[] executeGetCoursesSql(String dbName, String query) {

    }

    //  Write a code snippet that queries the SQL server to list all the students taking a certain course.
    private Student[] getStudentsInCourse(String courseCode) throws Exception {
        Student[] students = null;
        if (courseCode != null && !courseCode.isEmpty()) {
            String query = "SELECT id, first_name, last_name, mobile FROM students " +
                    "JOIN enrollment on enrollment.student_id = students.id where course_code= " + "\"" + courseCode + "\"";
            Student[] studentsAToM = executeGetStudentsInCourseSql(database1, query);
            Student[] studentsNToZ = executeGetStudentsInCourseSql(database2, query);
            if (studentsAToM != null && studentsNToZ != null) {
                students = Arrays.copyOf(studentsAToM, studentsAToM.length + studentsAToM.length);
                System.arraycopy(studentsNToZ, 0, students, studentsAToM.length, studentsNToZ.length);
            } else {
                if (studentsAToM != null) {
                    return studentsAToM;
                }
                if (studentsNToZ != null) {
                    return studentsNToZ;
                }
            }
        } else {
            throw new Exception("Argument courseCode is null or empty.");
        }
        return students;
    }

    //provided helper function
    private Student[] executeGetStudentsInCourseSql(String dbName, String query) {

    }

    //  Write a code snippet that upon student registration records the information into the right database.
    private void enrollStudent(String name, BigInteger number, Course[] courses) throws Exception {
        if (name != null && !name.isEmpty()) {
            String query = "Insert into enrollment(student_id, course_code, date_enrolled, final_grade) values (" + number + ", ";
            if ('A' <= name.charAt(0) && name.charAt(0) <= 'M') {
                for (Course course : courses) {
                    String que = query + "\"" + course.getCourseCode() + "\"," + "\"" + LocalDate.now() + "\"," + null + ")";
                    executeGetStudentsInCourseSql(database1, query);
                }
            } else {
                for (Course course : courses) {
                    String que = query + "\"" + course.getCourseCode() + "\"," + "\"" + LocalDate.now() + "\"," + null + ")";
                    executeGetStudentsInCourseSql(database2, query);
                }
            }
        } else {
            throw new Exception("Argument is null or empty.");
        }
    }

    //provided helper function
    private void executeEnrollStudentSql(String dbName, String query) {

    }

    // Write a code snippet that efficiently queries the information in the relevant databases and lists all the courses that are enrolled in by a given set of students.
    // This code should query only the databases required.
    private Map<Student, List<Course>> getCoursesForStudents(Student[] students) throws Exception {

        Map<Student, List<Course>> studentsWithCourses = null;
        if (students != null && students.length > 0) {
            String query = "SELECT students.id, students.first_name, students.last_name, students.mobile, courses.course_code, courses.name FROM courses JOIN enrollment on courses.course_code = enrollment.course_code\n" +
                    "JOIN students on enrollment.student_id = students.id where id in ";
            Arrays.sort(students, Comparator.comparing(Student::getFirstName));
            StringBuilder studentIdsAToM = new StringBuilder("(");
            StringBuilder studentIdsNToZ = new StringBuilder("(");
            for (Student student : students) {
                if ('A' <= student.getFirstName().charAt(0) && student.getFirstName().charAt(0) <= 'M') {
                    studentIdsAToM.append(student.getId()).append(",");
                } else {
                    studentIdsNToZ.append(student.getId()).append(",");
                }
            }
            studentIdsAToM.deleteCharAt(studentIdsAToM.length() - 1).append(")").append(" order by students.id;");
            studentIdsNToZ.deleteCharAt(studentIdsNToZ.length() - 1).append(")").append(" order by students.id;");

            studentsWithCourses = executeCoursesForStudents(database1, query + studentIdsAToM);
            studentsWithCourses.putAll(executeCoursesForStudents(database2, query + studentIdsNToZ));
        } else {
            throw new Exception("Argument is null or empty.");
        }
        return studentsWithCourses;
    }

    //Define the helper function that makes the API call to the sql server, following the same format as the helper functions provided previously.
    private Map<Student, List<Course>> executeCoursesForStudents(String database, String query) {

    }
}
